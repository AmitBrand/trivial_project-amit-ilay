#include "JsonResponsePacketSerializer.h"


char* JsonResponsePacketSerializer::serializeResponse(LoginResponse loginResponse)
{

	// Sample JSON data
	json jsonData = {
		{"status", loginResponse.status}
	};

    // Converting it to bytes
    return convertJsonToBytes(jsonData, LOGIN_CODE);
}

char* JsonResponsePacketSerializer::serializeResponse(SignupResoponse signupResponse)
{

    // Sample JSON data
    json jsonData = {
        {"status", signupResponse.status}
    };

    // Converting it to bytes
    return convertJsonToBytes(jsonData, SIGNUP_CODE);
}

char* JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorResponse)
{
    int code = 100;

    // Sample JSON data
    json jsonData = {
        {"message", errorResponse.message}
    };

    // Converting it to bytes
    return convertJsonToBytes(jsonData, code);
}

// Help Function
char* JsonResponsePacketSerializer::convertJsonToBytes(json jsonData, int code)
{
    std::string full_str;

    // Converting JSON to string
    std::string jsonString = jsonData.dump();

    // Saving the length
    int length = jsonString.length();

    // Convert code to a byte
    uint8_t codeByte = static_cast<uint8_t>(code);

    // Converting the length to four bytes
    uint8_t lengthBytes[4];
    lengthBytes[0] = (length >> 24) & 0xFF;
    lengthBytes[1] = (length >> 16) & 0xFF;
    lengthBytes[2] = (length >> 8) & 0xFF;
    lengthBytes[3] = length & 0xFF;

    // Appending the code byte to full_str
    full_str.push_back(codeByte);

    // Appending the length bytes to full_str
    for (int i = 0; i < 4; ++i) {
        full_str.push_back(lengthBytes[i]);
    }

    // Appending the JSON bytes to full_str
    full_str.append(jsonString);

    // Allocating memory for the var which will be restoring the bytes
    char* bytesStr = new char[full_str.size() * 8 + 1];
    size_t index = 0;

    // Converting each byte to its bit and appending it to bytesStr
    for (char byte : full_str) {
        std::string byteBits = std::bitset<8>(byte).to_string();
        for (char bit : byteBits) {
            bytesStr[index++] = bit;
        }
    }

    // Adding NULL at the end
    bytesStr[index] = '\0';

    return bytesStr;

}
