#pragma once

#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"

class LoginRequestHandler : public IRequestHandler
{
public:
    bool isRequestRelevant(RequestInfo ri) override;
    RequestResult handleRequest(RequestInfo ri) override;
private:
    RequestHandlerFactory& m_handlerFactory;

    RequestResult login(RequestInfo);
    RequestResult signup(RequestInfo);
};
