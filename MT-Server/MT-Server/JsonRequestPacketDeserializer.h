#pragma once

#include <iostream>
#include "json.hpp"
#include "Helper.h"
#include "Structs.h"

#define BITS_IN_BYTE 8
#define CODE_BYTE 1
#define LENGTH_BYTE 4


class JsonRequestPacketDeserializer
{

public:

	static LoginRequest deserializeLoginRequest(const char* buffer);
	static SignupRequest deserializeSignupRequest(const char* buffer);

};