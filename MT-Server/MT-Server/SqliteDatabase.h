#pragma once

#include "IDatabase.h"


class SqliteDatabase : public IDatabase
{

public:

	SqliteDatabase();
	~SqliteDatabase() = default;

	bool open();
	virtual bool close();

	virtual int doesUserExist(std::string username);
	virtual int doesPasswordMatch(std::string username, std::string password);

	virtual int addNewUser(std::string username, std::string password, std::string email);

private:

	sqlite3* _db;

	void createTables();
	bool executeSQL(std::string sql);
};