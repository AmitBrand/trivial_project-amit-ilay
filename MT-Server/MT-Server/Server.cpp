#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>

void Server::run()
{
	// Starting handling requests
	this->m_communicator.startHandlingRequests();
}
