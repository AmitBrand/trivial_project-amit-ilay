#include "JsonRequestPacketDeserializer.h" // Include necessary header file

using json = nlohmann::json; // Alias for nlohmann::json namespace


// Deserialize a login request from binary data
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const char* buffer)
{
	std::string fullReq(buffer); // Convert buffer to string
	std::string jsonStringInBinary = ""; // Initialize binary string for JSON data
	int i = 0;

	// Extract binary JSON data from the buffer
	for (i = BITS_IN_BYTE * (CODE_BYTE + LENGTH_BYTE); i < fullReq.size(); i++)
	{
		jsonStringInBinary += fullReq[i];
	}

	// Convert binary JSON data to text
	std::string jsonString = Helper::binaryToText(jsonStringInBinary);
	LoginRequest l; // Initialize LoginRequest object
	auto json = json::parse(jsonString); // Parse JSON string
	l.password = json["password"]; // Get password from JSON
	l.username = json["username"]; // Get username from JSON
	return l; // Return deserialized LoginRequest object
}

// Deserialize a signup request from binary data
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const char* buffer)
{
	std::string fullReq(buffer); // Convert buffer to string
	std::string jsonStringInBinary = ""; // Initialize binary string for JSON data
	int i = 0;

	// Extract binary JSON data from the buffer
	for (i = BITS_IN_BYTE * (CODE_BYTE + LENGTH_BYTE); i < fullReq.size(); i++)
	{
		jsonStringInBinary += fullReq[i];
	}

	// Convert binary JSON data to text
	std::string jsonString = Helper::binaryToText(jsonStringInBinary);
	SignupRequest s; // Initialize SignupRequest object
	auto json = json::parse(jsonString); // Parse JSON string
	s.password = json["password"]; // Get password from JSON
	s.username = json["username"]; // Get username from JSON
	s.email = json["email"]; // Get email from JSON

	return s; // Return deserialized SignupRequest object
}
