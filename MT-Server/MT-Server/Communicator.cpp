#include "Communicator.h"

Communicator::Communicator()
{
	WSADATA wsaData;
	WSAStartup(0x202, &wsaData);

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandlingRequests()
{
	struct sockaddr_in sa = { 0 };

	// Set up the sockaddr_in structure with server details
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	// Bind the server socket to the specified port
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	std::cout << "binded\n";

	// Start listening for incoming connections
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "listening...\n";

	while (true)
	{
		// Create a thread to accept incoming clients and detach it immediately
		std::thread mainT(&Communicator::bindAndListen, this);
		mainT.join();
	}
}


void Communicator::bindAndListen()
{
	// Accept incoming client connections
	std::cout << "Accepting client...\n";

	SOCKET client_socket = accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - accept");

	std::cout << "Client " << client_socket << " accepted!\n";

	// Create a thread to handle the communication with the accepted client and detach it
	std::thread t(&Communicator::handleNewClient, this, client_socket);
	t.detach();

}


void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{

		this->m_clients.insert({ clientSocket, new LoginRequestHandler() });

		// Recving request
		char rcv[MAX_SIZE] = { 0 };

		recv(clientSocket, rcv, sizeof(rcv) - 1, 0);

		std::string codeString = "";

		for (int i = 0; i < BITS_IN_BYTE; i++)
		{
			codeString += rcv[i];
		}

		// Getting msg code
		int code = Helper::binaryStringToDecimal(codeString);

		if (code == LOGIN_CODE)
		{
			LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(rcv);
		}
		
		else if (code == SIGNUP_CODE)
		{
			SignupRequest signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(rcv);
		}


		RequestInfo reqInfo;
		reqInfo.requestID = code;
		reqInfo.buffer = rcv;
		
		RequestResult reqRes = this->m_clients[clientSocket]->handleRequest(reqInfo);

		std::string resResp(reqRes.response);
		
		// Sending the result
		send(clientSocket, reqRes.response, resResp.size(), 0);;

	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}
}
