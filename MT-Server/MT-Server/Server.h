#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <WinSock2.h>
#include "Communicator.h"

class Server
{
public:
	void run();
private:
	Communicator m_communicator;

	
};