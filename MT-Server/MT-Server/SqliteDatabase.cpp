#include "SqliteDatabase.h"
#include <io.h>

using namespace std;

#define FILENAME "triviaDB.sqlite"

SqliteDatabase::SqliteDatabase()
{
    if (!open())
    {
        this->_db = nullptr; // Set the pointer to nullptr if opening failed
        cout << "Failed to open DB" << endl;
        return;
    }
}

bool SqliteDatabase::open()
{
    // Define the database file name
    string dbFileName = FILENAME;

    // Check if the database file exists
    int file_exist = _access(dbFileName.c_str(), 0);

    // Attempt to open the SQLite database connection
    int res = sqlite3_open(dbFileName.c_str(), &this->_db);

    // Check if the database was opened successfully
    if (res != SQLITE_OK)
    {
        this->_db = nullptr; // Set the pointer to nullptr if opening failed
        cout << "Failed to open DB" << endl;
        return false; // Return false to indicate failure
    }

    // Create tables if they don't exist
    createTables();

    return true; // Return true to indicate success
}

void SqliteDatabase::createTables()
{
    string sqlUsers = "CREATE TABLE IF NOT EXISTS USERS (USERNAME TEXT PRIMARY KEY, PASSWORD TEXT, EMAIL TEXT);";

    executeSQL(sqlUsers);
}

bool SqliteDatabase::executeSQL(std::string sql)
{
    char* errMsg = nullptr;

    // Execute the SQL statement
    int res = sqlite3_exec(this->_db, sql.c_str(), nullptr, nullptr, &errMsg);

    // Check for errors in executing the SQL statement
    if (res != SQLITE_OK)
    {
        std::cerr << "Error executing SQL: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }
    
    return true;
}

bool SqliteDatabase::close()
{
    // Close the SQLite database connection
    sqlite3_close(this->_db);
    this->_db = nullptr; // Set the pointer to nullptr after closing the connection
    
    return true;
}


// Define the callback function outside the class
int userExistenceCallback(void* data, int argc, char** argv, char** colNames) 
{
    bool* userExists = static_cast<bool*>(data);
    *userExists = (argc > 0 && argv[0] && *argv[0] == '1');
    return 0;
}

int SqliteDatabase::doesUserExist(std::string username) 
{

    bool userExists = false;

    // Prepare the SQL statement to check if the user exists
    std::string sql = "SELECT EXISTS (SELECT 1 FROM USERS WHERE USERNAME = '" + username + "');";

    // Execute the SQL statement
    int res = sqlite3_exec(this->_db, sql.c_str(), userExistenceCallback, &userExists, nullptr);
    if (res != SQLITE_OK) 
    {
        std::cerr << "Error executing SQL statement: " << sqlite3_errmsg(this->_db) << std::endl;
        return -1; // Return -1 to indicate an error
    }

    // Return whether the user exists
    return userExists ? 1 : 0;
}

// Define the callback function outside the class
int passwordMatchCallback(void* data, int argc, char** argv, char** colNames) 
{
    bool* passwordMatches = static_cast<bool*>(data);
    *passwordMatches = (argc > 0 && argv[0] && *argv[0] == '1');
    return 0;
}

int SqliteDatabase::doesPasswordMatch(std::string username, std::string password) 
{

    bool passwordMatches = false;

    // Prepare the SQL statement to check if the password matches
    string sql = "SELECT EXISTS (SELECT 1 FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "');";

    // Execute the SQL statement
    int res = sqlite3_exec(this->_db, sql.c_str(), passwordMatchCallback, &passwordMatches, nullptr);
    if (res != SQLITE_OK) 
    {
        std::cerr << "Error executing SQL statement: " << sqlite3_errmsg(this->_db) << std::endl;
        return -1; // Return -1 to indicate an error
    }

    // Return whether the password matches
    return passwordMatches ? 1 : 0;
}

int SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{

    string sql = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL) VALUES ('" + username + "', '" + password + "', '" + email + "');";

    return executeSQL(sql) ? 1 : 0;
}

