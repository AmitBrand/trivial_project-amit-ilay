#pragma once

#include <iostream>

class Helper
{
public:
	// Function to convert a binary string to text
	static std::string binaryToText(const std::string& binaryString);

	// Function to convert a binary string to a decimal number
	static int binaryStringToDecimal(const std::string& binaryString);
};