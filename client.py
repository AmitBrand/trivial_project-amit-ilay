import socket
import json

# Setting the server port and IP address
SERVER_PORT = 8826
SERVER_IP = '127.0.0.1'


def decimal_to_binary(decimal_num, num_bytes):
    # Convert decimal number to binary string
    binary_str = bin(decimal_num)[2:]

    # Calculate number of bits required for the binary representation
    num_bits = len(binary_str)

    # Calculate the number of leading zeros needed to pad the binary representation
    padding_zeros = num_bytes * 8 - num_bits

    # Pad the binary string with leading zeros if necessary
    padded_binary_str = '0' * padding_zeros + binary_str

    return padded_binary_str

    
    
def text_to_binary(text):
    binary_string = ''
    for char in text:
        binary_char = bin(ord(char))[2:].zfill(8)  # Convert character to ASCII binary
        binary_string += binary_char
    return binary_string
    
def serialize(code, data):
    full_msg = ""
    full_msg += decimal_to_binary(code, 1)
    json_string = json.dumps(data)  # Renamed 'json' to 'data'
    full_msg += decimal_to_binary(len(json_string), 4)
    full_msg += text_to_binary(json_string)
    return full_msg
    
    
def binary_to_decimal(binary_str):
    # Convert binary string to decimal number
    decimal_num = int(binary_str, 2)
    return decimal_num
    
    
def binary_to_text(binary_str):
    # Split binary string into 8-bit chunks
    chunks = [binary_str[i:i+8] for i in range(0, len(binary_str), 8)]
    
    # Convert each chunk to its corresponding ASCII character
    text = ''.join(chr(int(chunk, 2)) for chunk in chunks)
    
    return text
    

def deserialize(server_msg):

    data = binary_to_text(server_msg[40:])
    
    return json.loads(data)

    


def main():

    try:
        with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as sock:
            # Connecting to the server
            server_address = (SERVER_IP, SERVER_PORT)
            sock.connect(server_address)
            
            

            json_object = {
            "username": "ilay",
            "password": "12345",
            "email": "ilay@gmail.com"
            }
            
            client_msg = serialize(250, json_object)
    
            sock.sendall(client_msg.encode())
            
            # Receiving and printing a message from the server
            server_msg = sock.recv(1024)

            print(deserialize(server_msg.decode()))
         

    # Handling connection errors
    except ConnectionResetError and ConnectionRefusedError as e:
        # Handling ConnectionResetError and ConnectionRefusedError
        print(f"Error:{e}")

if __name__ == '__main__':
    main()
