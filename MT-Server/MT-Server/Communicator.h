#pragma once
#include <map>
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

#include <WinSock2.h>
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include "Helper.h"

#define PORT 8826
#define MAX_SIZE 1024

class Communicator
{
public:
	Communicator();
	~Communicator();

	void startHandlingRequests();
private:
	SOCKET _serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;

	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);

};