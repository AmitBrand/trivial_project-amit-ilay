#include "Helper.h"

// Function to convert a binary string to text
std::string Helper::binaryToText(const std::string& binaryString)
{
	std::string text; // Initialize text string

	// Iterate over the binary string in steps of 8
	for (size_t i = 0; i < binaryString.length(); i += 8)
	{
		// Extract each group of 8 bits
		std::string byte = binaryString.substr(i, 8);

		// Convert the binary byte to decimal
		int decimalValue = 0;
		for (size_t j = 0; j < byte.length(); ++j)
		{
			if (byte[j] == '1') // If the bit is 1
			{
				decimalValue += (1 << (7 - j)); // Calculate the decimal value of the byte
			}
		}

		// Convert the decimal value to its ASCII character and append to the text
		text += static_cast<char>(decimalValue);
	}

	return text; // Return the converted text
}

// Function to convert a binary string to a decimal number
int Helper::binaryStringToDecimal(const std::string& binaryString)
{
	int decimalValue = 0; // Initialize decimal value to 0
	int power = 0; // Initialize power to 0

	// Iterate over the string from right to left
	for (int i = binaryString.length() - 1; i >= 0; --i)
	{
		if (binaryString[i] == '1') // If the bit is 1
		{
			// Add 2^power to the decimal value if the bit is 1
			decimalValue += std::pow(2, power);
		}
		// Increment power for the next bit
		++power;
	}

	return decimalValue; // Return the calculated decimal value
}