#pragma once

#include <iostream>

struct LoginRequest
{
	std::string username;
	std::string password;
}

typedef LoginRequest;


struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
}
typedef SignupRequest;

struct LoginResponse
{
	unsigned int status;
}
typedef LoginResponse;

struct SignupResoponse
{
	unsigned int status;
}
typedef SignupResoponse;

struct ErrorResponse
{
	std::string message;
}
typedef ErrorResponse;