#pragma once
#include <ctime>
#include <iostream>

class IRequestHandler;

typedef struct RequestResult
{
    char* response;
    IRequestHandler* newHandler;
}
RequestResult;

typedef struct RequestInfo
{
    unsigned int requestID;
    std::time_t receivalTime;
    char* buffer;
}
RequestInfo;

class IRequestHandler
{
public:
    virtual bool isRequestRelevant(RequestInfo ri) = 0;
    virtual RequestResult handleRequest(RequestInfo ri) = 0;

    virtual ~IRequestHandler() {}
};
