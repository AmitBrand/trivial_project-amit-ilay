#include "LoginRequestHandler.h"


bool LoginRequestHandler::isRequestRelevant(RequestInfo ri)
{
	return ri.requestID == LOGIN_CODE || ri.requestID == SIGNUP_CODE;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo ri)
{
	char error[] = "Not Relevent";

	RequestResult requestRes;
	
	// If the request isn't relevent
	if (!isRequestRelevant(ri))
	{
		RequestResult err = {error, nullptr};

		// Returning error not relevant
		return err;
	}

	// Getting code
	int code = Helper::binaryStringToDecimal(std::string(ri.buffer, ri.buffer + BITS_IN_BYTE));

	if (code == LOGIN_CODE)
	{
		// status = 1
		LoginResponse loginReq = { 1 };
		char* loginRes = JsonResponsePacketSerializer::serializeResponse(loginReq);
		// Setting up fields
		requestRes.response = loginRes;
	}

	else if (code == SIGNUP_CODE)
	{
		// status = 1
		SignupResoponse signupReq = { 1 };
		char* signupRes = JsonResponsePacketSerializer::serializeResponse(signupReq);
		// Setting up fields
		requestRes.response = signupRes;
	}


	requestRes.newHandler = this;


	return requestRes;
}

RequestResult LoginRequestHandler::login(RequestInfo)
{

	return RequestResult();
}

RequestResult LoginRequestHandler::signup(RequestInfo)
{
	return RequestResult();
}
