#pragma once

#include <iostream>
#include <iomanip>
#include "json.hpp"
#include <vector>
#include <bitset>
#include "Structs.h"

#define LOGIN_CODE 200
#define SIGNUP_CODE 250

using json = nlohmann::json;


class JsonResponsePacketSerializer
{
public:
	static char* serializeResponse(LoginResponse loginResponse);
	static char* serializeResponse(SignupResoponse signupResponse);
	static char* serializeResponse(ErrorResponse errorResponse);
private:
	static char* convertJsonToBytes(json jsonData, int code);
};